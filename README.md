## HTML Tidy 8.x-1.x

### [](#about)About
The HTML Tidy module is intended to be a *development* tool used to format the
HTML returned by the Drupal rendering system before sending it to the client.
Predictable and consistently formatted markup may be able to lend aid to the
development process.

It should generally not be installed on production systems.

###[](#requirements)Requirements
This module requires the [Tidy](https://www.php.net/manual/en/book.tidy.php|tidy "Read the tidy book") php extension.

###[](#installing)Installing
The recommended way to install this module is via Composer.  Composer will
check for system prerequisites before allowing it to be installed.

It should be installed as a development dependency only.
`composer require drupal/tidy_html --dev`
###[](#faqs)Frequently Asked Questions
N/A
