<?php

namespace Drupal\tidy_html;

/**
 * An interface that formats HTML source code.
 *
 * @package Drupal\tidy
 */
interface TidyInterface {

  /**
   * Generates and returns a formatted version of the provided HTML markup.
   *
   * @param string $html
   *   The HTML to format.
   *
   * @return string
   *   A formatted version of the provided HTML.
   */
  public function format($html);

}
