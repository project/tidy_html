<?php

namespace Drupal\tidy_html;

/**
 * This class wraps an instance of a Tidy extension object.
 *
 * Note - if this module is *somehow* installed on a system without tidy or
 * if the module is installed and then the tidy php extension is removed from
 * the system, then a fallback \Drupal\tidy_html\ExtensionPlaceholder service
 * is injected instead.  The extension placeholder does no formatting, and the
 * cache needs to be cleared in order for the switchover to occur.
 *
 * @see https://www.php.net/manual/en/book.tidy.php
 *
 * @package Drupal\tidy
 */
class Tidy implements TidyInterface {

  /**
   * The tidy service.
   *
   * @var \tidy|\Drupal\tidy_html\ExtensionPlaceholder
   */
  protected $tidy;

  /**
   * Tidy constructor.
   *
   * @param mixed $tidy
   *   A tidy object or a placeholder object.
   */
  public function __construct($tidy) {
    $this->tidy = $tidy;
  }

  /**
   * {@inheritdoc}
   */
  public function format($html) {

    // @see https://api.html-tidy.org/tidy/tidylib_api_5.8.0/tidy_quickref.html
    // @TODO: Make these presets configurable.
    $options = [
      'indent' => '2',
      'indent-spaces' => 2,
      'output-html' => TRUE,
      'wrap' => 0,
      'output-encoding' => TRUE,
      'escape-scripts' => FALSE,
      'drop-empty-elements' => FALSE,
      'drop-empty-paras' => FALSE,
    ];
    return $this->tidy->repairString($html, $options);
  }

}
