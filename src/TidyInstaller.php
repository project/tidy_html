<?php

namespace Drupal\tidy_html;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * This class contains delegate methods for the install file.
 *
 * @package Drupal\tidy_html
 */
class TidyInstaller {

  use StringTranslationTrait;

  /**
   * Delegate method for hook_requirements().
   *
   * @param string $phase
   *   The phase in which requirements are checked.
   *
   * @return array
   *   The requirements array for the tidy_html module.
   */
  public function getRequirements($phase) {
    $requirements = [
      'tidy_html' => [
        'title' => $this->t('Tidy HTML'),
      ],
    ];
    if (!extension_loaded('tidy')) {
      $requirements['tidy_html']['severity'] = REQUIREMENT_ERROR;
      if ($phase === 'install') {
        $requirements['tidy_html']['description'] = $this->t('This system is missing the tidy php extension.  The Tidy HTML module cannot be installed without it.');
      }
      elseif ($phase === 'update') {
        $requirements['tidy_html']['description'] = $this->t('This system is missing the tidy php extension.  The Tidy HTML module cannot be updated without it.');
      }
      else {
        $requirements['tidy_html']['description'] = $this->t('This system is missing the tidy php extension.  Install the missing extension or uninstall the Tidy HTML module.');
      }
    }
    else {
      $requirements['tidy_html']['description'] = $this->t('The tidy php extension is installed.  HTML responses will be formatted as configured.');
      $requirements['tidy_html']['value'] = phpversion('tidy');
      $requirements['tidy_html']['severity'] = REQUIREMENT_OK;
    }

    return $requirements;
  }

}
