<?php

namespace Drupal\tidy_html;

/**
 * This is a placeholder class that's used as a fallback dependency.
 *
 * It is used in situations when the php tidy extension is not present on the
 * system.  If this fallback was not present, then the site would fail with
 * fatal errors and this module might not be able to be normally uninstalled.
 *
 * @package Drupal\tidy_html
 */
class ExtensionPlaceholder {

  /**
   * Simply returns the provided HTML back to the caller.
   *
   * @param string $html
   *   The HTML to beautify.
   * @param array $options
   *   This parameter is unused.
   *
   * @return string
   *   The $html parameter.
   */
  public function repairString($html, array $options) {
    return $html;
  }

}
