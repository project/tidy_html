<?php

namespace Drupal\tidy_html\EventSubscriber;

use Drupal\tidy_html\TidyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * A response filter that beautifies HTML responses.
 *
 * Only HTML responses should be processed by this filter.  This is determined
 * exclusively by the Content-Type header of the response.
 *
 * @package Drupal\tidy\EventSubscriber
 */
class TidyResponseFilter implements EventSubscriberInterface {

  /**
   * The tidy service.
   *
   * @var \Drupal\tidy_html\TidyInterface
   */
  protected $tidy;

  /**
   * Creates a new tidy response filter object.
   *
   * @param \Drupal\tidy_html\TidyInterface $tidy
   *   The tidy service.
   */
  public function __construct(TidyInterface $tidy) {
    $this->tidy = $tidy;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [KernelEvents::RESPONSE => 'onResponse'];
  }

  /**
   * Subscriber callback for the kernel response event.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event that was dispatched.
   */
  public function onResponse(ResponseEvent $event) {

    $response = $event->getResponse();
    $content_type = $response->headers->get('Content-Type');
    if (stripos($content_type, 'text/html') === FALSE) {
      return;
    }

    $response->setContent($this->tidy->format($response->getContent()));
  }

}
