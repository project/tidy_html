<?php

namespace Drupal\tidy_html;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * This service provider will swap out the 'tidy_php_extension' service.
 *
 * If the tidy extension is loaded, then the default 'ExtensionPlaceholder'
 * class in the service will be replaced with an instance of the \tidy class.
 *
 * @TODO: Rethink global naming convention. This class name can't change.
 *
 * @package Drupal\tidy_html
 */
class TidyHtmlServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::register($container);

    // @TODO: Research alternatives to extension_loaded.  It's ugly to mock.
    if (!extension_loaded('tidy')) {
      return;
    }

    // If tidy is loaded, adjust the service to use it.
    $container
      ->getDefinition('tidy_php_extension')
      ->setClass(\tidy::class);
  }

}
