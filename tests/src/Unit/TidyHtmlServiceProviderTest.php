<?php

namespace Drupal\Tests\tidy_html\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\TidyHtmlServiceProvider;
use Symfony\Component\DependencyInjection\Definition;

// Required to mock the '\extension_loaded' function.
require_once realpath(__DIR__ . '/../global_test_vars.php');
require_once realpath(__DIR__ . '/../global_function_mocks.php');

/**
 * Test cases for the TidyHtmlServiceProvider class.
 *
 * @package Drupal\Tests\tidy_html\Unit
 *
 * @group tidy_html
 */
class TidyHtmlServiceProviderTest extends UnitTestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\tidy_html\TidyHtmlServiceProvider
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->instance = new TidyHtmlServiceProvider();
  }

  /**
   * Test case for the alter method with the tidy extension loaded.
   *
   * @covers \Drupal\tidy_html\TidyHtmlServiceProvider::alter
   */
  public function testAlterWithExtension() {
    global $_tidy_html_mock_extension_loaded;
    $_tidy_html_mock_extension_loaded = TRUE;

    $definition_mock = $this->getMockBuilder(Definition::class)
      ->disableOriginalConstructor()
      ->getMock();

    $definition_mock
      ->expects($this->once())
      ->method('setClass')
      ->with('tidy');

    /* @var $container_mock \Drupal\Core\DependencyInjection\ContainerBuilder|\PHPUnit\Framework\MockObject\MockObject */
    $container_mock = $this->getMockBuilder(ContainerBuilder::class)
      ->disableOriginalConstructor()
      ->getMock();

    $container_mock
      ->expects($this->once())
      ->method('getDefinition')
      ->willReturn($definition_mock);

    $this->instance->alter($container_mock);
  }

  /**
   * Test case for the alter method without the tidy extension loaded.
   *
   * @covers \Drupal\tidy_html\TidyHtmlServiceProvider::alter
   */
  public function testAlterWithoutExtension() {
    global $_tidy_html_mock_extension_loaded;
    $_tidy_html_mock_extension_loaded = FALSE;

    /* @var $container_mock \Drupal\Core\DependencyInjection\ContainerBuilder|\PHPUnit\Framework\MockObject\MockObject */
    $container_mock = $this->getMockBuilder(ContainerBuilder::class)
      ->disableOriginalConstructor()
      ->getMock();

    $container_mock
      ->expects($this->never())
      ->method('getDefinition');

    $this->instance->alter($container_mock);
  }

}
