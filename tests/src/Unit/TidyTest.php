<?php

namespace Drupal\Tests\tidy_html\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\Tidy;
use Drupal\tidy_html\ExtensionPlaceholder;

/**
 * Unit test cases for the tidy service.
 *
 * @package Drupal\Tests\tidy_html\Unit
 *
 * @group tidy_html
 */
class TidyTest extends UnitTestCase {

  /**
   * A mocked version of the tidy php extension.
   *
   * @var \tidy|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $mockTidy;

  /**
   * The subject under test.
   *
   * @var \Drupal\tidy_html\TidyInterface
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mockTidy = $this->getMockBuilder(ExtensionPlaceholder::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->instance = new Tidy($this->mockTidy);
  }

  /**
   * Test case for the beautify method.
   *
   * @covers \Drupal\tidy_html\Tidy::format
   */
  public function testBeautify() {
    $this->mockTidy
      ->expects($this->once())
      ->method('repairString')
      ->willReturn('Beautiful Mock Data');

    $expected = 'Beautiful Mock Data';
    $actual = $this->instance->format($expected);

    $this->assertEquals($expected, $actual);
  }

}
