<?php

namespace Drupal\Tests\tidy_html\Unit\EventSubscriber;

use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\EventSubscriber\TidyResponseFilter;
use Drupal\tidy_html\TidyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Unit test cases for the tidy response filter.
 *
 * @package Drupal\Tests\tidy_html\Unit\EventSubscriber
 *
 * @group tidy_html
 */
class TidyResponseFilterTest extends UnitTestCase {

  /**
   * The mocked tidy service.
   *
   * @var \Drupal\tidy_html\TidyInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $tidyMock;

  /**
   * The subject under test.
   *
   * @var \Drupal\tidy_html\EventSubscriber\TidyResponseFilter
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->tidyMock = $this->getMockBuilder(TidyInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->instance = new TidyResponseFilter($this->tidyMock);
  }

  /**
   * Prepares a mocked filter response event.
   *
   * @param string $content_type
   *   The content type header string to set up.
   *
   * @return \Symfony\Component\HttpKernel\Event\ResponseEvent|\PHPUnit\Framework\MockObject\MockObject
   *   The mock event.
   */
  protected function prepareMockEvent($content_type) {
    $headers_mock = $this->getMockBuilder(ResponseHeaderBag::class)
      ->disableOriginalConstructor()
      ->getMock();

    $headers_mock
      ->expects($this->once())
      ->method('get')
      ->with('Content-Type')
      ->willReturn($content_type);

    /* @var $response_mock \Symfony\Component\HttpFoundation\Response|\PHPUnit\Framework\MockObject\MockObject */
    $response_mock = $this->getMockBuilder(Response::class)
      ->disableOriginalConstructor()
      ->getMock();

    // We should only modify HTML responses and NEVER any others.
    if ($content_type === 'text/html') {
      $response_mock
        ->expects($this->once())
        ->method('setContent');
    }
    else {
      $response_mock
        ->expects($this->never())
        ->method('setContent');
    }

    $response_mock->headers = $headers_mock;


    $event_mock = new ResponseEvent($this->createMock(KernelInterface::class), $this->createMock(Request::class), 0, $response_mock);

    return $event_mock;
  }

  /**
   * Test case for the getSubscribedEvents method.
   *
   * @covers \Drupal\tidy_html\EventSubscriber\TidyResponseFilter::getSubscribedEvents
   */
  public function testGetSubscribedEvents() {
    $expected = [KernelEvents::RESPONSE => 'onResponse'];
    $actual = TidyResponseFilter::getSubscribedEvents();

    $this->assertEquals($expected, $actual);
  }

  /**
   * Test case for the onResponse method for HTML responses.
   *
   * @covers \Drupal\tidy_html\EventSubscriber\TidyResponseFilter::onResponse
   */
  public function testOnResponseHtml() {
    $mock_event = $this->prepareMockEvent('text/html');
    $this->instance->onResponse($mock_event);
  }

  /**
   * Test case for the onResponse method for non-HTML responses.
   *
   * @covers \Drupal\tidy_html\EventSubscriber\TidyResponseFilter::onResponse
   */
  public function testOnResponseNonHtml() {
    $mock_event = $this->prepareMockEvent('application/json');
    $this->instance->onResponse($mock_event);
  }

}
