<?php

namespace Drupal\Tests\tidy_html\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\TidyInstaller;

require_once dirname(dirname(dirname(__DIR__))) . '/tidy_html.install';

/**
 * Unit test for hook_requirements().
 *
 * @package Drupal\Tests\tidy_html\Unit
 *
 * @group tidy_html
 */
class HookHtmlRequirementsTest extends UnitTestCase {

  /**
   * The mocked installer service.
   *
   * @var \Drupal\tidy_html\TidyInstaller|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $tidyInstallerMock;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->tidyInstallerMock = $this->getMockBuilder(TidyInstaller::class)
      ->disableOriginalConstructor()
      ->getMock();

    $container = new ContainerBuilder();
    $container->set('tidy.installer', $this->tidyInstallerMock);

    \Drupal::setContainer($container);
  }

  /**
   * Test case for the requirements hook for the install phase.
   *
   * @covers ::\tidy_html_requirements
   */
  public function testTidyInstallRequirements() {
    $this->tidyInstallerMock
      ->expects($this->never())
      ->method('getRequirements');

    \tidy_html_requirements('install');
  }

  /**
   * Test case for the requirements hook for the update phase.
   *
   * @covers ::\tidy_html_requirements
   */
  public function testTidyUpdateRequirements() {
    $this->tidyInstallerMock
      ->expects($this->never())
      ->method('getRequirements')
      ->willReturn([]);

    \tidy_html_requirements('update');
  }

  /**
   * Test case for the requirements hook for the runtime phase.
   *
   * @covers ::\tidy_html_requirements
   */
  public function testTidyRuntimeRequirements() {
    $this->tidyInstallerMock
      ->expects($this->once())
      ->method('getRequirements')
      ->willReturn([]);

    \tidy_html_requirements('runtime');
  }

}
