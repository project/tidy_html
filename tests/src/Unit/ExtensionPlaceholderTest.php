<?php

namespace Drupal\Tests\tidy_html\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\ExtensionPlaceholder;

/**
 * Test cases for the ExtensionPlaceholder class.
 *
 * @package Drupal\Tests\tidy_html\Unit
 *
 * @group tidy_html
 */
class ExtensionPlaceholderTest extends UnitTestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\tidy_html\ExtensionPlaceholder
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->instance = new ExtensionPlaceholder();
  }

  /**
   * Test case for the repairString method.
   *
   * @covers \Drupal\tidy_html\ExtensionPlaceholder::repairString
   */
  public function testRepairString() {
    $expected = 'This is to be expected.';
    $actual = $this->instance->repairString($expected, []);

    $this->assertEquals($expected, $actual);
  }

}
