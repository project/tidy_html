<?php

namespace Drupal\Tests\tidy_html\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\tidy_html\TidyInstaller;

// Required to mock the '\extension_loaded' function.
require_once dirname(__DIR__) . '/global_test_vars.php';
require_once dirname(__DIR__) . '/global_function_mocks.php';


/**
 * Unit test for the installer delegate service.
 *
 * @package Drupal\Tests\tidy_html\Unit
 *
 * @group tidy_html
 */
class TidyInstallerTest extends UnitTestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\tidy_html\TidyInstaller
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $this->instance = new TidyInstaller();
  }

  /**
   * Toggles the mocked state of the tidy php extension on or off.
   *
   * @param bool $status
   *   TRUE to enable the extension or FALSE to disable it.
   */
  protected function toggleTidyExtension($status) {
    global $_tidy_html_mock_extension_loaded;
    $_tidy_html_mock_extension_loaded = $status;
  }

  /**
   * Negative test case for the getRequirements method for the install phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetInstallRequirementsNegative() {
    $this->toggleTidyExtension(FALSE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_ERROR,
        'description' => 'This system is missing the tidy php extension.  The Tidy HTML module cannot be installed without it.',
      ],
    ];
    $actual = $this->instance->getRequirements('install');

    $this->assertEquals($expected, $actual);
  }

  /**
   * Positive test case for the getRequirements method for the install phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetInstallRequirementsPositive() {
    $this->toggleTidyExtension(TRUE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_OK,
        'value' => phpversion('tidy'),
        'description' => 'The tidy php extension is installed.  HTML responses will be formatted as configured.',
      ],
    ];
    $actual = $this->instance->getRequirements('install');

    $this->assertEquals($expected, $actual);
  }

  /**
   * Negative test case for the getRequirements method for the update phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetUpdateRequirementsNegative() {
    $this->toggleTidyExtension(FALSE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_ERROR,
        'description' => 'This system is missing the tidy php extension.  The Tidy HTML module cannot be updated without it.',
      ],
    ];
    $actual = $this->instance->getRequirements('update');

    $this->assertEquals($expected, $actual);
  }

  /**
   * Positive test case for the getRequirements method for the update phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetUpdateRequirementsPositive() {
    $this->toggleTidyExtension(TRUE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_OK,
        'value' => phpversion('tidy'),
        'description' => 'The tidy php extension is installed.  HTML responses will be formatted as configured.',
      ],
    ];
    $actual = $this->instance->getRequirements('update');

    $this->assertEquals($expected, $actual);
  }

  /**
   * Negative test case for the getRequirements method for the runtime phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetRuntimeRequirementsNegative() {
    $this->toggleTidyExtension(FALSE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_ERROR,
        'description' => 'This system is missing the tidy php extension.  Install the missing extension or uninstall the Tidy HTML module.',
      ],
    ];
    $actual = $this->instance->getRequirements('runtime');

    $this->assertEquals($expected, $actual);
  }

  /**
   * Positive test case for the getRequirements method for the runtime phase.
   *
   * @covers \Drupal\tidy_html\TidyInstaller::getRequirements
   */
  public function testGetRuntimeRequirementsPositive() {
    $this->toggleTidyExtension(TRUE);

    $expected = [
      'tidy_html' => [
        'title' => 'Tidy HTML',
        'severity' => REQUIREMENT_OK,
        'value' => phpversion('tidy'),
        'description' => 'The tidy php extension is installed.  HTML responses will be formatted as configured.',
      ],
    ];
    $actual = $this->instance->getRequirements('runtime');

    $this->assertEquals($expected, $actual);
  }

}
