<?php

/**
 * @file
 * Contains global variables for the tidy_html testing package.
 */

$_tidy_html_mock_extension_loaded = FALSE;

// Copied from install.inc.
const REQUIREMENT_OK = 0;
const REQUIREMENT_ERROR = 2;
