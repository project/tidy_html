<?php

/**
 * @file
 * Contains global function mock overrides for the tidy_html package.
 */

namespace Drupal\tidy_html;

/**
 * Overrides \extension_loaded.
 */
function extension_loaded($name) {
  global $_tidy_html_mock_extension_loaded;
  return $_tidy_html_mock_extension_loaded;
}
